> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 A2

## Brian Conrad

### Assignment Requirements:

1. Screenshot of running application's first user interface
2. Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of First User Interface*:

![User Interface One](http://i.imgur.com/9wOJp1S.png "Interface 1 Screenshot")

*Screenshot of Second User Interface*:

![User Interface Two](http://i.imgur.com/lPGslZt.png "Interface 2 Screenshot")
